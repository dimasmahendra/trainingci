<link href="<?= base_url('asset/jquery.dataTables.min.css') ?>" rel="stylesheet">

<?php if($this->session->flashdata('error')){?> 
<div class="alert alert-danger">  
<?php echo $this->session->flashdata('error')?> 
    </div><?php } ?>

<div class="col-lg-12 well">
	<div class="row">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-8 form-group">
					<input type="text" id="link" name="link" placeholder="Enter Link Here.." class="form-control">
				</div>
				<div class="col-sm-2 form-group">
					<button class="btn btn-md btn-info generate">Generate</button>	
				</div>
			</div>								
		</div> 
	</div>
</div>
<div class="col-lg-12 greyarea">
	<div class="row">
		<div class="display_info"></div>
	</div>
</div>

<form role="form" method="post" action="<?= base_url('User/logout')?>">
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-2 form-group pull-right">
				<button type="submit" class="btn btn-md btn-warning">Log Out</button>	
			</div>
		</div>								
	</div>
</form>

<div class="col-lg-12 greyareaa">
	<div class="row">
		<table id="linkTable" class="display" cellspacing="0" width="100%">
	        <thead>
	          <tr>
	            <th>ID</th>
	            <th>Code</th>  
	            <th>Link</th>
	            <th>Action</th>
	          </tr>
	        </thead>     
	  	</table> 
	</div>
</div>

<script src="<?= base_url('asset/jquery-2.2.0.min.js') ?>" rel="stylesheet"></script>
<script src="<?= base_url('asset/jquery.dataTables.min.js') ?>" rel="stylesheet"></script>
<script src="<?= base_url('asset/dataTables.bootstrap.min.js') ?>" rel="stylesheet"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#linkTable').DataTable( {
        "processing": true,
        "serverSide": true,
		"paging": false,
        "ajax": "<?php echo base_url(); ?>dashboard/getTable",
         "columns": [
		    { "data": "id" },
		    { "data": "code" },
		    { "data": "link" },
		    { "data": "action" },
		  ]
    } );
} );
</script>

<script>
$(document).on("click", ".generate", function () {
	var link = $('#link').val();

   	if(link)
   	{
      	$.ajax({
            type: 'post',
            url:'<?php echo base_url(); ?>Dashboard/inputLink',
            data: 
            {
              link:link,
            },
            success: function (response)
            {              
              $( '.display_info' ).html(response);
            }
      	});
   }
    
   else
   {
    	$( '.display_info' ).html("Please Enter Some Words");
   }
		
});    
</script>