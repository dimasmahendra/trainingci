<link href="<?= base_url('asset/jquery.dataTables.min.css') ?>" rel="stylesheet">

<h1 class="well">Statistic</h1>
<div class="col-lg-12 greyareaa">
	<a href="<?= base_url('Dashboard/index')?>"><button type="submit" class="btn btn-md btn-success">Back</button></a>
	<div class="row">
		<table id="linkTable" class="display" cellspacing="0" width="100%">
	        <thead>
	          <tr>
	            <th>No</th>
	            <th>Time</th>  
	            <th>IP</th>
	            <th>Browser</th>
	          </tr>
	        </thead>     
	  	</table> 
	</div>
</div>

<script src="<?= base_url('asset/jquery-2.2.0.min.js') ?>" rel="stylesheet"></script>
<script src="<?= base_url('asset/jquery.dataTables.min.js') ?>" rel="stylesheet"></script>
<script src="<?= base_url('asset/dataTables.bootstrap.min.js') ?>" rel="stylesheet"></script>

<script type="text/javascript">
  $(document).ready(function() {
	    $('#linkTable').DataTable( {
	        "processing": true,
	        "serverSide": true,
			"paging": false,
	        "ajax": "<?php echo base_url(); ?>Statistic/getLinkStats/<?php echo $this->uri->segment(3); ?>",
	         "columns": [
			    { "data": "no" },
			    { "data": "time" },
			    { "data": "ip" },
			    { "data": "browser" },
			  ]
	    } );
	} );
</script>