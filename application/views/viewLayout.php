<!DOCTYPE html>
<html>
<head>
	<title>Training CI</title>

	<?php $this->load->view($head);?>

</head>
<body>

<div class="container">
    <?php $this->load->view($content);?>
</div>

</body>
</html>