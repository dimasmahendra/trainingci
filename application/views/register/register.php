<h1 class="well">Register</h1>

<?php if($this->session->flashdata('error')){?> 
<div class="alert alert-danger">  
<?php echo $this->session->flashdata('error')?> 
    </div><?php } ?>

<div class="col-lg-12 well">
<div class="row">
	<form role="form" method="post" action="<?= base_url('User/new')?>">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-6 form-group">
					<label>First Name</label>
					<input type="text" name="firstname" placeholder="Enter First Name Here.." class="form-control" required>
				</div>
				<div class="col-sm-6 form-group">
					<label>Last Name</label>
					<input type="text" name="lastname" placeholder="Enter Last Name Here.." class="form-control" required>
				</div>
			</div>											
			<div class="form-group">
				<label>Username</label>
				<input type="text" name="username" placeholder="Enter Your Username Here.." class="form-control" required>
			</div>	
			<div class="form-group">
				<label>Password</label>
				<input type="password" name="password" placeholder="Enter Password Address Here.." class="form-control" required>
			</div>	
			<div class="form-group">
				<label>Email Address</label>
				<input type="email" name="email" placeholder="Enter Email Address Here.." class="form-control" required>
			</div>	
			<button type="submit" class="btn btn-lg btn-info">Submit</button>					
		</div>
	</form> 
	</div>
</div>
</div>