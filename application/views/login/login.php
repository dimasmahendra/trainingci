<h1 class="well">Login</h1>

<?php if($this->session->flashdata('error')){?> 
<div class="alert alert-danger">  
<?php echo $this->session->flashdata('error')?> 
    </div><?php } ?>

<?php if($this->session->flashdata('success')){?> 
<div class="alert alert-success">  
<?php echo $this->session->flashdata('success')?> 
    </div><?php } ?>

<div class="col-lg-12 well">
<div class="row">
	<form role="form" method="post" action="<?= base_url('User/login')?>">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-6 form-group">
					<label>Username</label>
					<input type="text" name="username" placeholder="Enter Username Here.." class="form-control" required>
				</div>
				<div class="col-sm-6 form-group">
					<label>Password</label>
					<input type="password" name="password" placeholder="Enter Password Here.." class="form-control" required>
				</div>
			</div>
			<button type="submit" class="btn btn-lg btn-info">Submit</button>	
			<div class="pull-right">
				<a href="<?= base_url('User/page')?>"> Buat Akun Baru </a>
			</div>
		</div>
	</form>			
	</div>
</div>