<?php 
class Link_stat_model extends CI_Model {

    public function getLinkStats($Idlink) {
        $this->db->where('link_id', $Idlink);          
        $q = $this->db->get('link_stats');
        $data = $q->result_array();
        $no = 1;
        
        foreach ($data as $key => $value) {
            $hasil['data'][] = array( 
                'no' => $no++,
                'time' => date("m/d/Y h:i:s A T",$value['time']),
                'ip' => $value['ip'],
                'browser' => $value['browser']
            );
        }
        return json_encode($hasil);
    }
}