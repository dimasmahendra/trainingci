<?php 
class Shortlink_model extends CI_Model {

    public function convertIntToShortCode() {

        $chars = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        );

        shuffle($chars);

        $num_chars = 5;
        $token = '';

        for ($i = 0; $i < $num_chars; $i++){ // <-- $num_chars instead of $len
            $token .= $chars[mt_rand(0, $num_chars)];
        }
        return $token;
    }

    public function insertLink($insertLink) {
        $this->db->insert('link', $insertLink);
        if($this->db->affected_rows() > 0) {
            return TRUE;
        }
        else {
            return False;
        }
    }

    public function getLink($code) {
        $this->db->where('code', $code);          
        $q = $this->db->get('link');
        $data['id'] = $q->row()->id;
        $data['link'] = $q->row()->link;

        return $data;
    }

    public function getUserAgent() {

        $this->load->library('user_agent');
        $data['browser'] = $this->agent->browser();
        $data['ip'] = $this->input->ip_address();

        return $data;
    }

    public function insertLinkStats($insertLink_Stats) {
        $this->db->insert('link_stats', $insertLink_Stats);
        if($this->db->affected_rows() > 0) {
            return TRUE;
        }
        else {
            return False;
        }
    }

    public function getStatics($id) { 
        $this->db->where('user_id', $id);   
        $q = $this->db->get('link');
        $data = $q->result_array();

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $hasil['data'][] = array( 
                    'id' => $value['id'],
                    'code' => $value['code'],
                    'link' => $value['link'],
                    'action' => '<a href= ' . base_url() . 'Statistic/index/' . $value['id'] . '> view </a>'
                );
            }
        }
        else {
            $hasil['data'][] = array( 
                'id' => '',
                'code' => '',
                'link' => '',
                'action' => ''
            );
        }
        return json_encode($hasil);
    }
}