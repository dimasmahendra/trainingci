<?php 
class User_model extends CI_Model {

    public function getlogin($register)
    {
        $this->db->where('username', $register['username']);          
        $q = $this->db->get('user');
        $data = $q->result_array();

        if (!empty($data)) {
            if (password_verify($register['password'], $data[0]['password'])) {
                return array('id' => $data[0]['id']);
            } else {
                return FALSE;
            }            
        }
        else {
            return FALSE;
        }                
    }

    public function registerNew($register)
        {           
            $this->db->where('username', $register['username']);
            $this->db->where('email', $register['email']);            
            $q = $this->db->get('user');
            $data = $q->result_array();
            //print_r(!empty($data));die();            
            if (!empty($data)) {
                return False;
            }
            else {
                $this->db->insert('user', $register);
                if($this->db->affected_rows() > 0) {
                    $insert_id = $this->db->insert_id();
                    return array('id' => $insert_id);
                }
                else {
                    return False;
                }
            }                
        }
}