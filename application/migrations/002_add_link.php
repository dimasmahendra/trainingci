<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_link extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'auto_increment'    =>  TRUE
                        ),
                        'user_id' => array(
                                'type' => 'INT',
                                'constraint' => 50
                        ),
                        'code' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 50
                        ),
                        'link' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 255
                        ),
                        'expired' => array(
                                'type' => 'INT',
                                'constraint' => 10
                        ),
                        'created' => array(
                                'type' => 'INT',
                                'constraint' => 10
                        ),
                        'deleted' => array(
                                'type' => 'INT',
                                'constraint' => 10
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('link');
        }

        public function down()
        {
                $this->dbforge->drop_table('link');
        }
}