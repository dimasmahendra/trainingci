<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_user extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'auto_increment'    =>  TRUE
                        ),
                        'username' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 50
                        ),
                        'password' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 255
                        ),
                        'email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 50
                        ),
                        'first_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 50
                        ),
                        'last_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 100
                        ),
                        'created' => array(
                                'type' => 'INT',
                                'constraint' => 10
                        ),
                        'updated' => array(
                                'type' => 'INT',
                                'constraint' => 10
                        ),
                        'deleted' => array(
                                'type' => 'INT',
                                'constraint' => 10
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('user');
        }

        public function down()
        {
                $this->dbforge->drop_table('user');
        }
}