<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_link_stats extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'auto_increment'    =>  TRUE
                        ),
                        'link_id' => array(
                                'type' => 'INT',
                                'constraint' => 5
                        ),
                        'ip' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 50
                        ),
                        'browser' => array(
                                'type' => 'VARCHAR',
                                'constraint' => 50
                        ),
                        'time' => array(
                                'type' => 'INT',
                                'constraint' => 10
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('link_stats');
        }

        public function down()
        {
                $this->dbforge->drop_table('link_stats');
        }
}