<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistic extends MY_Controller {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('Link_stat_model');       
    }

    public function index()
	{
		$data['head'] = 'viewHeader';
		$data['content'] = 'statistic/viewStatistic';
		$this->load->view('viewLayout', $data);
	}

	public function getLinkStats($Idlink)
	{
		$get = $this->Link_stat_model->getLinkStats($Idlink);
		echo $get;
	}
}
