<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('Shortlink_model');       
    }

	public function index()
	{
		$data['head'] = 'viewHeader';
		$data['content'] = 'dashboard/dashboard';
		$this->load->view('viewLayout', $data);
	}

	public function inputLink()
	{
		$id   = $this->session->userdata('logged_in');
		$code = $this->Shortlink_model->convertIntToShortCode();

		$insertLink = array(
			'user_id' => $id['id'],
			'code' => $code,
			'link' => $this->input->post('link'),
			'expired' => strtotime('+24 hours', strtotime( date('Y-m-d H:i:s') )),
			'created' => strtotime(date('Y-m-d H:i:s'))
		);

		$result = $this->Shortlink_model->insertLink($insertLink);

		if ($result == TRUE) {
			echo "<a href='http://localhost/test/Dashboard/gotolink/". $code ."' target='_blank'> http://localhost/test/Dashboard/gotolink/" . $code . "</a>";
		}
		else {
			echo "There are an Error, Please Contact Your IT Support";
		}
	}

	public function gotolink($code)
	{		
		$result = $this->Shortlink_model->getLink($code);
		$user = $this->Shortlink_model->getUserAgent();

		$insertLink_Stats = array(
			'link_id' => $result['id'],
			'ip' => $user['ip'],
			'browser' => $user['browser'],
			'time' => strtotime(date('Y-m-d H:i:s'))
		);

		$hasil = $this->Shortlink_model->insertLinkStats($insertLink_Stats);
		if ($hasil == TRUE) {
			redirect($result['link']);
		}
		else {
			$this->session->set_flashdata('error', 'There are an Error, Please Contact Your IT Support');
			redirect('Dashboard/index', 'refresh');
		}
	}

	public function getTable()
	{		
		$id   = $this->session->userdata('logged_in');
		$get = $this->Shortlink_model->getStatics($id['id']);
        echo $get;
	}
}
