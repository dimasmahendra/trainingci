<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
	    parent::__construct();
	    $this->load->model('User_model');       
    }

	public function index()
	{
		if(empty($_SESSION['logged_in'])){
			$data['head'] = 'viewHeader';
			$data['content'] = 'login/login';
			$this->load->view('viewLayout', $data);
		}
		else {
			redirect('Dashboard','refresh');
		}		
	}

	public function login()
	{
		$register = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
		);

		$result = $this->User_model->getlogin($register);
		if ($result) {
			$this->session->set_userdata('logged_in',$result);
			redirect('Dashboard/index');
		}
		else {			
			$this->session->set_flashdata('error', 'Login Failed');
			redirect('User/index', 'refresh');
		}		
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		$this->session->set_flashdata('success', 'See You ..');              
        redirect('User','refresh');
	}

	public function new()
	{
		$register = array(
			'username' => $this->input->post('username'),
			'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
			'email' => $this->input->post('email'),
			'first_name' => $this->input->post('firstname'),
			'last_name' => $this->input->post('lastname'),
			'created' => strtotime(date('Y-m-d H:i:s'))
		);

		$result = $this->User_model->registerNew($register);		
		if ($result) {
			$this->session->set_userdata('logged_in',$result);
			redirect('Dashboard/index');
		}
		else {
			
			$this->session->set_flashdata('error', 'There are an Error, Please Contact Your IT Support');
			redirect('Register/index', 'refresh');
		}		
	}

	public function page()
	{
		if(empty($_SESSION['logged_in'])){
			$data['head'] = 'viewHeader';
			$data['content'] = 'register/register';
			$this->load->view('viewLayout', $data);
		}
		else {
			redirect('Dashboard','refresh');
		}	
	}
}
